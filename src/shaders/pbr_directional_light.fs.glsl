#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform vec3 uLightDirection;
uniform vec3 uLightIntensity;

uniform sampler2D uBaseColorTexture;
uniform vec4 uBaseColorFactor;

uniform float uMetallicFactor;
uniform float uRoughnessFactor;
uniform sampler2D uMetallicRoughnessTexture;

uniform vec3 uEmissiveFactor;
uniform sampler2D uEmissiveTexture;

uniform int uOcclusionEnabled;
uniform float uOcclusionFactor;
uniform sampler2D uOcclusionTexture;

out vec3 fColor;

// Constants
const float GAMMA = 2.2;
const float INV_GAMMA = 1. / GAMMA;
const float M_PI = 3.141592653589793;
const float M_1_PI = 1.0 / M_PI;

vec3 LINEARtoSRGB(vec3 color) { return pow(color, vec3(INV_GAMMA)); }

vec4 SRGBtoLINEAR(vec4 srgbIn) { return vec4(pow(srgbIn.xyz, vec3(GAMMA)), srgbIn.w); }

float shlickFactor(float x) {
   float x2 = x * x;
   return x2 * x2 * x;
}

float cdot(vec3 x, vec3 y) {
   return clamp(dot(x, y), 0.0, 1.0);
}

void main()
{
   const vec3 ior = vec3(0.04);
   const vec3 black = vec3(0.0);
   const vec3 white = vec3(1.0);

   vec3 N = normalize(vViewSpaceNormal);
   vec3 V = normalize(-vViewSpacePosition);
   vec3 L = uLightDirection;
   vec3 H = normalize(L + V);

   // Dots
   float VdotH = cdot(V, H);
   float VdotN = cdot(V, N);
   float LdotN = cdot(L, N);
   float NdotH = cdot(N, H);

   // Color
   vec4 baseColorFromTexture = SRGBtoLINEAR(texture(uBaseColorTexture, vTexCoords));
   vec4 baseColor = uBaseColorFactor * baseColorFromTexture;

   // Metallic & Roughness
   vec4 metallicRougnessFromTexture = texture(uMetallicRoughnessTexture, vTexCoords);
   vec3 metallic = vec3(uMetallicFactor * metallicRougnessFromTexture.b);
   float roughness = uRoughnessFactor * metallicRougnessFromTexture.g;

   vec3 c_diff = mix(baseColor.rgb * (white - ior.r), black, metallic);
   vec3 f0 = mix(vec3(ior), baseColor.rgb, metallic);
   float alpha = roughness * roughness;
   float alpha2 = alpha * alpha;

   vec3 f = f0 + (white - f0) * shlickFactor(1.0 - VdotH);

   float visDenominator = LdotN * sqrt(VdotN * VdotN * (1 - alpha2) + alpha2) + VdotN * sqrt(LdotN * LdotN * (1 - alpha2) + alpha2);
   float Vis = visDenominator > 0.0 ? 0.5 / visDenominator : 0.0;

   float DDenominator = NdotH * NdotH * (alpha2 - 1.0) + 1.0;
   float D = M_1_PI * alpha2 / (DDenominator * DDenominator);

   vec3 f_specular = f * Vis * D;
   vec3 f_diffuse = (1.0 - f) * M_1_PI * c_diff;

   vec3 emissive = SRGBtoLINEAR(texture(uEmissiveTexture, vTexCoords)).rgb * uEmissiveFactor;

   vec3 color = (f_diffuse + f_specular) * uLightIntensity * LdotN;
   color += emissive;

   if (uOcclusionEnabled == 1) {
      float occlusion = texture(uOcclusionTexture, vTexCoords).r;
      color = mix(color, color * occlusion, uOcclusionFactor);
   }
   fColor = LINEARtoSRGB(color);
}

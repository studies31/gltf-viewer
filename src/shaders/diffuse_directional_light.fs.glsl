#version 330

in vec3 vViewSpacePosition;
in vec3 vViewSpaceNormal;
in vec2 vTexCoords;

uniform vec3 uLightDirection; // Should be normalized
uniform vec3 uLightIntensity;

#define PI 3.1415

out vec3 fColor;

void main()
{
   vec3 fr = vec3(1 / PI);
   float cosw = dot(uLightDirection, normalize(vViewSpaceNormal));
   fColor = fr * uLightIntensity * cosw;
}